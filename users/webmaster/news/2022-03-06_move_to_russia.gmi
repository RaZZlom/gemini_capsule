# Перемещение инстанса на территорию России. / Moving the instance to Russia.

## RU

В эти тяжёлые времена я приветствую вас товарищи.

Мной было принято тяжёлое решение переместить инстанс на хостинг находящийся на территории Российской Федерации.

Краткая сводка проделанной работы:

* Misskey инстанс переехал от Scaleway на FirstVDS.
* Gemini капсулы тоже.
* Все медиафайлы скопированы из Scaleway S3 в Selectel S3.
* Деньги с Patreon выводятся. Прошу вас НЕ подписываться на него так как аккаунт там будет закрыт. Аккаунт на Boosty.to будет создан на следующих выходных.
* Оплатить Scaleway за прошлый и часть этого месяца я не могу по не зависящим от меня причинам. Жду последующего наказания с их стороны.

### Обновление от 2022-03-19

* Были обновлены правила инстанса.
* Обновлена информация о способах поддержки quietplace.xyz

### Обновление от 2022-03-20

* Обновлены ссылки на медиа файлы. Теперь мы на 100% используем нового поставщика s3.
* Эмоджи были сломаны и удалены. Часть восстановлена.

Наш автобус продолжает следовать в ад.

## EN

In these difficult times I greet you comrades.

I have made the difficult decision to move the instance to hosting on the territory of the Russian Federation.

A brief summary of the work done:

* Misskey instance moved from Scaleway to FirstVDS.
* Gemini capsules too.
* All media files copied from Scaleway S3 to Selectel S3.
* Patreon money is withdrawn. Please do NOT subscribe to it as your account there will be closed. An account at Boosty.to will be created next weekend.
* Paying Scaleway for last month and part of this month I can't due to reasons beyond my control. I am awaiting further punishment from them.

### Update 2022-03-19

* Instance rules have been updated.
* Updated the ways to support quietplace.xyz

### Update 2022-03-20

* Media file links have been updated. We are now 100% using the new s3 vendor.
* Emoji were broken and deleted. Some have been restored.

Our bus continues to go to hell.

