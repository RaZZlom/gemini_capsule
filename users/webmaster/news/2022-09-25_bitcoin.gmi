# Bitcoin

## RU

Вы все давно просили и наконец-то мы смогли! Начиная с этого дня вы можете помочь quietplace.xyz переведя нам немного bitcoin монет.

```bitcoin adress
bc1qgrwe9zqsxq99ztrv7clha72rynk2yf6tl7kkp0
```

Можно переводить вот на этот адрес.

Большое вам спасибо!

P.S. Отдельное спасибо @blank@qoto.org за помощь.

## EN

You all have been asking for a long time and we finally managed! Starting from this day you can help quietplace.xyz by transferring us some bitcoin coins.

```bitcoin adress
bc1qgrwe9zqsxq99ztrv7clha72rynk2yf6tl7kkp0
```

You can transfer to this address.

Thank you very much!

P.S. Special thanks to @blank@qoto.org for your help.
